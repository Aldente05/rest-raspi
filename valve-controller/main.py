import RPi.GPIO as GPIO
import time, sys, os
GPIO.setwarnings

R1 = 25
FLOW_SENSOR = 23

#Setup GPIO output channel
GPIO.setmode(GPIO.BCM)
GPIO.setup(R1, GPIO.OUT)
GPIO.setup(FLOW_SENSOR, GPIO.IN, pull_up_down = GPIO.PUD_UP)

#Perubahan waktu LED dengan jam(H), menit(M)
#Definisi GPIO on
R1onH = 21
R1onM = 23
global count
global calc
calc = 0
count = 0
liter = 0
setliter = 1

def pemberitahuan(StartOnH,StartOnM):
    print ('Waktu hurung na LED 1 : ', StartOnH, ':', StartOnM)

def countPulse(channel):
    global count
    count = count+1
    calc = round((count / 543), 3) # this rotation per 1 Liter Water
    

def main(channel):
    global calc
    #Untuk mendapatkan waktu tina raspberry
    waktu = list(time.localtime())
    jam = waktu[3]
    menit = waktu[4]

    #LED 1 Start logic
    # LED 1 ON
    if jam == 22:
        if menit == 11:
            GPIO.output(R1, GPIO.LOW)
            #print 'Hurung euy'

    # LED 1 Off
    if calc == 1:
        GPIO.output(R1, GPIO.HIGH)
        print ('Pareum euy')


GPIO.add_event_detect(FLOW_SENSOR, GPIO.FALLING, callback=countPulse)

print(calc)

while True:
    #main()
    #print (calc)
    
    try:
        time.sleep(1)
    except KeyboardInterrupt:
        print
        '\ncaught keyboard interrupt!, bye'
        GPIO.cleanup()
        sys.exit()
